#!/bin/bash

# List of user agents
USER_AGENTS=(
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36"
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36"
  "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36"
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15"
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 13_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15"
)

# Create directory for results
mkdir -p parkrun_results

# Initialize associative array to store first finishes
declare -A first_finishes

# Counter for rotating user agents
counter=0

# Retry function
retry_request() {
  local url="$1"
  local user_agent="$2"
  local max_attempts=3
  local attempt=1
  local http_code=202
  while [ $attempt -le $max_attempts ] && [ $http_code -eq 202 ]; do
    echo "Attempt $attempt: Fetching $url"
    http_code=$(curl -s -o /dev/null -w "%{http_code}" -H "user-agent: $user_agent" "$url")
    ((attempt++))
    sleep 1  # Wait for a short period before retrying
  done
  return $http_code
}

# Loop through URLs from 548 to 558
for i in {1..563}; do
  URL="https://www.parkrun.com.au/torrens/results/${i}/"
  USER_AGENT="${USER_AGENTS[$counter]}"
  ((counter++))
  if [ $counter -ge ${#USER_AGENTS[@]} ]; then
    counter=0
  fi
  # Fetch the results page and extract the first place finisher
  retry_request "$URL" "$USER_AGENT"
  HTTP_CODE=$?
  # Echo the HTTP return code to stdout
  echo "HTTP return code for event ${i}: ${HTTP_CODE}"

  if [[ $HTTP_CODE -ge 200 && $HTTP_CODE -lt 300 ]]; then
    # If the request was successful (HTTP code 2XX), continue processing
    FIRST_PLACE=$(curl -s -H "user-agent: $USER_AGENT" "$URL" | grep -Eo '<div class="compact"><a[^>]+>[^<]+' | sed -n '1p' | sed 's/.*>//')
    # Save the first place finisher to individual event file
    echo "${FIRST_PLACE}"
    echo "${FIRST_PLACE}" > "parkrun_results/event_${i}_first_place.txt"
    # Save all results to individual event file
    curl -s -H "user-agent: $USER_AGENT" "$URL" | grep -Eo '<div class="compact"><a[^>]+>[^<]+' | sed 's/.*>//' > "parkrun_results/event_${i}_results.txt"

    # Update first finishes array
    if [ -n "${first_finishes[$FIRST_PLACE]}" ]; then
      first_finishes[$FIRST_PLACE]=$(( ${first_finishes[$FIRST_PLACE]} + 1 ))
    else
      first_finishes[$FIRST_PLACE]=1
    fi
  fi
done

# Create summary file for first finishes
echo "Parkrunner,Number of First Finishes" > "parkrun_results/first_finishes_summary.txt"
for parkrunner in "${!first_finishes[@]}"; do
  echo "$parkrunner,${first_finishes[$parkrunner]}" >> "parkrun_results/first_finishes_summary.txt"
done

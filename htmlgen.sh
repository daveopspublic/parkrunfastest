# Function to generate HTML page
generate_html() {
    # Start the HTML content
    html_content="<html>
<head>
<meta charset='UTF-8'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<title>Parkrun Results</title>
<style>
  body {
    font-family: Arial, sans-serif;
    margin: 20px;
  }
  h1 {
    text-align: center;
  }
  table {
    border-collapse: collapse;
    width: 80%;
    margin: 0 auto;
  }
  th, td {
    border: 1px solid #ddd;
    padding: 8px;
    text-align: left;
  }
  th {
    background-color: #f2f2f2;
  }
</style>
</head>
<body>
  <h1>Parkrun Results</h1>
  <table>
    <thead>
      <tr>
        <th>Parkrunner</th>
        <th>Number of First Finishes</th>
      </tr>
    </thead>
    <tbody>"
echo "$html_content" > "public/index.html"


    # Read each line of the summary file and generate table rows
    awk -F',' 'NR>1 {print "<tr><td>" $1 "</td><td>" $2 "</td></tr>"}' "public/firsts.txt" >> "public/index.html"

    # Finish the HTML content
    html_content="</tbody>
  </table>
</body>
</html>"

    echo $html_content
    # Write the HTML content to the file
    echo "$html_content" >> "public/index.html"
}
generate_html

#!/bin/bash

# Read the file into an array
while IFS=, read -r name number; do
    if [[ ! -z "$name" && ! -z "$number" ]]; then
        data+=("$name,$number")
    fi
done < parkrun_results/first_finishes_summary.txt

echo ${data[1]}

# Sort the array based on the number in descending order
#sorted_data=($(printf "%s\n" "${data[@]}" | sort -t',' -k2nr))
IFS=$'\n' sorted_data=($(sort -t',' -k2nr <<<"${data[*]}"))
echo ${sorted_data[1]}

# Print the sorted data
echo "" > public/firsts.txt
for item in "${sorted_data[@]}"; do
    echo "$item" >> public/firsts.txt
done

